<?php

namespace GalleryBundle\Controller;

use GalleryBundle\Entity\Album;
use GalleryBundle\Entity\Image;
use GalleryBundle\Form\ImageType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Image controller.
 *
 * @Route("profile/image")
 */
class ImageController extends OwnerController
{
    /**
     * Creates a new image entity.
     *
     * @Route("/new/{albumId}", name="profile_image_new")
     * @Method({"GET", "POST"})
     * @param Request $request
     * @param int     $albumId
     * @return RedirectResponse|Response
     */
    public function newAction(Request $request, $albumId)
    {
        $em = $this->getDoctrine()->getManager();
        $album = $em->getRepository(Album::class)->find($albumId);

        $image = (new Image())
            ->setOwner($this->getUser())
            ->setAlbum($album);

        $form = $this->createForm(ImageType::class, $image);
        $form->add('create', SubmitType::class, ['attr' => ['class' => 'btn btn-primary'],])
             ->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->get('gallery.image_service')
                 ->handleImage($image, $form->get('image')->getData());

            $em->persist($image);
            $em->flush($image);

            return $this->redirectToRoute('profile_album_show', ['id' => $albumId]);
        }

        return $this->render(
            '@Gallery/Default/form.html.twig',
            [
                'image' => $image,
                'form'  => $form->createView(),
            ]
        );
    }

    /**
     * Displays a form to edit an existing image entity.
     *
     * @Route("/{id}/edit", name="profile_image_edit")
     * @Method({"GET", "POST"})
     * @param Request $request
     * @param Image   $image
     * @return RedirectResponse|Response
     */
    public function editAction(Request $request, Image $image)
    {
        $editForm = $this->createForm(ImageType::class, $image);
        $editForm->remove('image')
                 ->add('edit', SubmitType::class, ['attr' => ['class' => 'btn btn-primary'],])
                 ->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('profile_album_show', ['id' => $image->getAlbum()->getId()]);
        }

        return $this->render(
            '@Gallery/Default/form.html.twig',
            [
                'image' => $image,
                'form'  => $editForm->createView(),
            ]
        );
    }

    /**
     * Deletes a image entity.
     *
     * @Route("/{id}/delete", name="profile_image_delete")
     * @param Image $image
     * @return RedirectResponse
     */
    public function deleteAction(Image $image)
    {
        if ($this->canEdit($image)) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($image);
            $em->flush($image);
        }

        return $this->redirectToRoute('profile_album_show', ['id' => $image->getAlbum()->getId()]);
    }
}

<?php

namespace GalleryBundle\Controller;

use GalleryBundle\Entity\Album;
use GalleryBundle\Form\AlbumType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

/**
 * Album controller.
 *
 * @Route("profile/album")
 */
class AlbumController extends OwnerController
{
    /**
     * Lists all album entities.
     *
     * @Route("/", name="profile_album_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        if (null === $this->getUser()) {
            return $this->redirectToRoute('index');
        }

        return $this->render(
            '@Gallery/Default/album_index.html.twig',
            [
                'album'    => null,
                'albums'   => $this->getDoctrine()
                                   ->getManager()
                                   ->getRepository('GalleryBundle:Album')
                                   ->findBy(['owner' => $this->getUser(), 'parent' => null]),
                'images'   => [],
                'can_edit' => true,
            ]
        );
    }

    /**
     * Creates a new album entity.
     *
     * @Route("/new/{albumId}", name="profile_album_new")
     * @Method({"GET", "POST"})
     * @param Request $request
     * @param null    $albumId
     * @return RedirectResponse|Response
     */
    public function newAction(Request $request, $albumId = null)
    {
        $parent = null;
        $em = $this->getDoctrine()->getManager();

        if (null !== $albumId) {
            $parent = $em->getRepository('GalleryBundle:Album')->find($albumId);
        }

        $album = (new Album())
            ->setOwner($this->getUser())
            ->setParent($parent);

        $form = $this->createForm(AlbumType::class, $album);
        $form->remove('image')
             ->add('create', SubmitType::class, ['attr' => ['class' => 'btn btn-primary'],])
             ->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em->persist($album);
            $em->flush($album);

            return $this->redirectToRoute('profile_album_show', ['id' => $album->getId()]);
        }

        return $this->render(
            '@Gallery/Default/form.html.twig',
            [
                'album' => $album,
                'form'  => $form->createView(),
            ]
        );
    }

    /**
     * Finds and displays a album entity.
     *
     * @Route("/{id}", name="profile_album_show")
     * @Method("GET")
     * @param Album $album
     * @return Response
     */
    public function showAction(Album $album)
    {
        if (!$this->canEdit($album) && $album->getPrivate()) {
            throw new AccessDeniedException();
        }

        return $this->render(
            '@Gallery/Default/album_index.html.twig',
            [
                'album'    => $album,
                'albums'   => $album->getAlbums(),
                'images'   => $album->getImages(),
                'can_edit' => $this->canEdit($album),
            ]
        );
    }

    /**
     * Displays a form to edit an existing album entity.
     *
     * @Route("/{id}/edit", name="profile_album_edit")
     * @Method({"GET", "POST"})
     * @param Request $request
     * @param Album   $album
     * @return RedirectResponse|Response
     */
    public function editAction(Request $request, Album $album)
    {
        $editForm = $this->createForm(AlbumType::class, $album);
        $editForm->add('edit', SubmitType::class, ['attr' => ['class' => 'btn btn-primary'],])
                 ->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('profile_album_show', ['id' => $album->getId()]);
        }

        return $this->render(
            '@Gallery/Default/form.html.twig',
            [
                'album' => $album,
                'form'  => $editForm->createView(),
            ]
        );
    }

    /**
     * Deletes a album entity.
     *
     * @Route("/{id}/delete", name="profile_album_delete")
     * @param Request $request
     * @param Album   $album
     * @return RedirectResponse
     */
    public function deleteAction(Request $request, Album $album)
    {
        if ($this->canEdit($album)) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($album);
            $em->flush($album);
        }

        return $this->redirectToRoute('profile_album_index');
    }
}

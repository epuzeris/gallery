<?php

/**
 * @copyright C UAB NFQ Technologies 2016
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

namespace GalleryBundle\Controller;


use GalleryBundle\Entity\Album;
use GalleryBundle\Entity\Image;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

abstract class OwnerController extends Controller
{
    /**
     * @param Album|Image $object
     * @return bool
     */
    protected function canEdit($object)
    {
        $user = $this->getUser();

        return null !== $user && $user->getId() === $object->getOwner()->getId();
    }
}

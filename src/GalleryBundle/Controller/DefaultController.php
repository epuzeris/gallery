<?php

namespace GalleryBundle\Controller;

use GalleryBundle\Entity\Image;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="index")
     */
    public function indexAction()
    {
        $images = $this->get('doctrine.orm.entity_manager')
                       ->getRepository('GalleryBundle:Image')
                       ->findBy([
                           'private' => false,
                       ]);

        return $this->render('GalleryBundle:Default:index.html.twig', [
            'images' => $this->get('gallery.image_service')->filterPrivate($images),
        ]);
    }

    /**
     * @Route("/user/{username}/{albumId}", name="user_gallery")
     * @param int $albumId
     * @return Response
     */
    public function galleryAction($albumId)
    {
        return $this->render('GalleryBundle:Default:index.html.twig', [
            'images' => $this->get('doctrine.orm.entity_manager')
                             ->getRepository('GalleryBundle:Image')
                             ->findBy([
                                 'album' => $albumId,
                             ]),
        ]);
    }
}

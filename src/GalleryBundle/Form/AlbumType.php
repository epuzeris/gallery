<?php

namespace GalleryBundle\Form;

use Doctrine\ORM\EntityRepository;
use GalleryBundle\Entity\Album;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AlbumType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        /** @var Album $album */
        $album = $builder->getData();
        $builder
            ->add('name')
            ->add('description')
            ->add(
                'private',
                null,
                [
                    'label' => 'Private',
                ]
            );

        if (null !== $album->getId()) {
            $builder
                ->add(
                    'image',
                    EntityType::class,
                    [
                        'class'         => 'GalleryBundle\Entity\Image',
                        'query_builder' => function (EntityRepository $er) use ($album) {
                            $qb = $er->createQueryBuilder('i');

                            return $qb->where($qb->expr()->eq('i.album', $album->getId()));
                        },
                        'choice_label'  => 'name',
                    ]
                );
        }
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                'data_class' => 'GalleryBundle\Entity\Album',
            ]
        );
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'gallerybundle_album';
    }


}

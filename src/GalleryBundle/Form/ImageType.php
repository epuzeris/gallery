<?php

namespace GalleryBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints as Assert;

class ImageType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name')
            ->add('description')
            ->add('private')
            ->add(
                'image',
                FileType::class,
                [
                    'label'       => 'Image',
                    'mapped'      => false,
                    'constraints' => [
                        new Assert\Image(
                            [
                                'minWidth'  => 200,
                                'minHeight' => 200,
                                'maxWidth'  => 2000,
                                'maxHeight' => 2000,
                            ]
                        ),
                    ],
                    'attr'        => [
                        'accept' => 'image/*',
                    ],
                ]
            );
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                'data_class' => 'GalleryBundle\Entity\Image',
            ]
        );
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'gallerybundle_image';
    }


}

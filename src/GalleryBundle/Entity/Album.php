<?php

namespace GalleryBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Album
 *
 * @ORM\Table(name="album")
 * @ORM\Entity(repositoryClass="GalleryBundle\Repository\AlbumRepository")
 */
class Album
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var Album
     * @ORM\ManyToOne(targetEntity="GalleryBundle\Entity\Album")
     * @ORM\JoinColumn(name="parent_id", referencedColumnName="id", nullable=true, onDelete="CASCADE")
     */
    private $parent;

    /**
     * @var Album[]
     * @ORM\OneToMany(targetEntity="GalleryBundle\Entity\Album", mappedBy="parent")
     */
    private $albums;

    /**
     * @var Image[]
     * @ORM\OneToMany(targetEntity="GalleryBundle\Entity\Image", mappedBy="album")
     */
    private $images;

    /**
     * @var Image
     * @ORM\ManyToOne(targetEntity="GalleryBundle\Entity\Image")
     * @ORM\JoinColumn(name="image_id", referencedColumnName="id", nullable=true)
     */
    private $image;

    /**
     * @var User
     * @ORM\ManyToOne(targetEntity="GalleryBundle\Entity\User")
     * @ORM\JoinColumn(name="owner_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $owner;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", nullable=true)
     */
    private $description;

    /**
     * @var bool
     *
     * @ORM\Column(name="private", type="boolean")
     */
    private $private;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->albums = new \Doctrine\Common\Collections\ArrayCollection();
        $this->images = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Album
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Album
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get private
     *
     * @return bool
     */
    public function getPrivate()
    {
        return $this->private;
    }

    /**
     * Set private
     *
     * @param boolean $private
     *
     * @return Album
     */
    public function setPrivate($private)
    {
        $this->private = $private;

        return $this;
    }

    /**
     * Get parent
     *
     * @return \GalleryBundle\Entity\Album
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * Set parent
     *
     * @param \GalleryBundle\Entity\Album $parent
     *
     * @return Album
     */
    public function setParent(\GalleryBundle\Entity\Album $parent = null)
    {
        $this->parent = $parent;

        return $this;
    }

    /**
     * Add album
     *
     * @param \GalleryBundle\Entity\Album $album
     *
     * @return Album
     */
    public function addAlbum(\GalleryBundle\Entity\Album $album)
    {
        $this->albums[] = $album;

        return $this;
    }

    /**
     * Remove album
     *
     * @param \GalleryBundle\Entity\Album $album
     */
    public function removeAlbum(\GalleryBundle\Entity\Album $album)
    {
        $this->albums->removeElement($album);
    }

    /**
     * Get albums
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAlbums()
    {
        return $this->albums;
    }

    /**
     * Add image
     *
     * @param \GalleryBundle\Entity\Image $image
     *
     * @return Album
     */
    public function addImage(\GalleryBundle\Entity\Image $image)
    {
        $this->images[] = $image;

        return $this;
    }

    /**
     * Remove image
     *
     * @param \GalleryBundle\Entity\Image $image
     */
    public function removeImage(\GalleryBundle\Entity\Image $image)
    {
        $this->images->removeElement($image);
    }

    /**
     * Get images
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getImages()
    {
        return $this->images;
    }

    /**
     * Get owner
     *
     * @return \GalleryBundle\Entity\User
     */
    public function getOwner()
    {
        return $this->owner;
    }

    /**
     * Set owner
     *
     * @param \GalleryBundle\Entity\User $owner
     *
     * @return Album
     */
    public function setOwner(\GalleryBundle\Entity\User $owner = null)
    {
        $this->owner = $owner;

        return $this;
    }

    /**
     * Get image
     *
     * @return \GalleryBundle\Entity\Image
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Set image
     *
     * @param \GalleryBundle\Entity\Image $image
     *
     * @return Album
     */
    public function setImage(\GalleryBundle\Entity\Image $image = null)
    {
        $this->image = $image;

        return $this;
    }
}

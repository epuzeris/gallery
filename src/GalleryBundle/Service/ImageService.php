<?php

namespace GalleryBundle\Service;


use GalleryBundle\Entity\Album;
use GalleryBundle\Entity\Image;
use Gregwar\ImageBundle\Services\ImageHandling;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class ImageService
{
    /**
     * @var string
     */
    private $imagesDirectory;
    /**
     * @var ImageHandling
     */
    private $imageHandling;

    /**
     * ImageService constructor.
     * @param string        $imagesDirectory
     * @param ImageHandling $imageHandling
     */
    public function __construct($imagesDirectory, ImageHandling $imageHandling)
    {
        $this->imagesDirectory = $imagesDirectory;
        $this->imageHandling = $imageHandling;
    }

    /**
     * @param Image              $image
     * @param UploadedFile|mixed $file
     */
    public function handleImage(&$image, $file)
    {
        if ($file instanceof UploadedFile) {
            $baseName = md5(uniqid());
            $fileName = $baseName.'.'.$file->guessExtension();
            $thumbnailName = $baseName.'_thumb.'.$file->guessExtension();

            $file->move(
                $this->imagesDirectory,
                $fileName
            );
            $this->createThumbnail($fileName, $thumbnailName);

            $image->setLink($fileName);
            $image->setThumbnail($thumbnailName);
        }
    }

    /**
     * @param Image[] $images
     * @return Image[]
     */
    public function filterPrivate($images)
    {
        foreach ($images as $key => $image) {
            $isVisible = $this->getAlbumVisibility($image->getAlbum());
            if (!$isVisible || $image->getPrivate()) {
                unset($images[$key]);
            }
        }

        return $images;
    }

    /**
     * @param string $fileName
     * @param string $thumbnailName
     */
    public function createThumbnail($fileName, $thumbnailName)
    {
        $this
            ->imageHandling
            ->open($this->imagesDirectory.'/'.$fileName)
            ->cropResize(200, 200)
            ->save($this->imagesDirectory.'/'.$thumbnailName);
    }

    /**
     * @param Album $album
     * @return bool
     */
    private function getAlbumVisibility($album)
    {
        if ($album->getPrivate()) {
            return false;
        }

        if (null === $album->getParent()) {
            return true;
        }

        return $this->getAlbumVisibility($album->getParent());
    }
}

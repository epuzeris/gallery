<?php

namespace GalleryBundle\Twig;

use GalleryBundle\Entity\Album;
use GalleryBundle\Entity\Image;
use Symfony\Component\Asset\Packages;

class HelperExtension extends \Twig_Extension
{
    /**
     * @var
     */
    private $imagesDirectory;
    /**
     * @var Packages
     */
    private $packages;

    /**
     * HelperExtension constructor.
     * @param string   $imagesDirectory
     * @param Packages $packages
     */
    public function __construct($imagesDirectory, Packages $packages)
    {
        $this->imagesDirectory = $imagesDirectory;
        $this->packages = $packages;
    }

    public function getFunctions()
    {
        return [
            new \Twig_SimpleFunction('getImage', [$this, 'getImage']),
            new \Twig_SimpleFunction('getFullImage', [$this, 'getFullImage']),
        ];
    }

    public function getImage($object)
    {
        if ($object instanceof Album) {
            $object = $this->getAlbumImage($object);
        }

        if ($object instanceof Image) {
            return $this->getImageLink($object->getThumbnail());
        }

        return Image::DEFAULT_IMAGE;
    }

    private function getImageLink($fileName)
    {
        return $this->packages->getUrl($this->imagesDirectory.'/'.$fileName);
    }

    public function getFullImage($object)
    {
        if ($object instanceof Album) {
            $object = $this->getAlbumImage($object);
        }

        if ($object instanceof Image) {
            return $this->getImageLink($object->getLink());
        }

        return Image::DEFAULT_IMAGE;
    }

    /**
     * @param Album $album
     * @return Image
     */
    private function getAlbumImage($album)
    {
        if (null !== $album->getImage()) {
            return $album->getImage();
        }
        if ($album->getImages()->count() > 0) {
            return $album->getImages()->get(0);
        }
        if ($album->getAlbums()->count() > 0) {
            foreach ($album->getAlbums() as $album) {
                $image = $this->getAlbumImage($album);
                if (null !== $image) {
                    return $image;
                }
            }
        }

        return null;
    }

    public function getName()
    {
        return 'app_extension';
    }
}

<?php


/**
 * Inherited Methods
 * @method void wantToTest($text)
 * @method void wantTo($text)
 * @method void execute($callable)
 * @method void expectTo($prediction)
 * @method void expect($prediction)
 * @method void amGoingTo($argumentation)
 * @method void am($role)
 * @method void lookForwardTo($achieveValue)
 * @method void comment($description)
 * @method \Codeception\Lib\Friend haveFriend($name, $actorClass = null)
 *
 * @SuppressWarnings(PHPMD)
 */
class AcceptanceTester extends \Codeception\Actor
{
    use _generated\AcceptanceTesterActions;

    public function amLogin($username, $password)
    {
        $this->amOnPage('/login');
        $this->submitForm('form', [
            '_username' => $username,
            '_password' => $password,
        ]);
        $this->amOnPage('/profile');
        $this->see('Username: '.$username);
    }

    public function amUser($firstName, $password)
    {
        $this->amOnPage('/register');
        $this->submitForm('.fos_user_registration_register', [
            'fos_user_registration_form' => [
                'email'         => $firstName.'@test.com',
                'username'      => $firstName,
                'plainPassword' => [
                    'first'  => $password,
                    'second' => $password,
                ],
            ],
        ]);
        $this->see('Username: '.$firstName);
    }

    public function createAlbum($albumName, $text)
    {

        //Album
        $this->amOnPage('/profile/album/');
        $this->see('Create a new album');
        $this->click('Create a new album');
        $this->see('Create');

        $this->submitForm('form', [
            'gallerybundle_album' => [
                'name'        => $albumName,
                'description' => $text,
            ],
        ]);
        $this->see($albumName);
    }
}

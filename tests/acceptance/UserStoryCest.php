<?php

class UserStoryCest
{
    /** @var \Faker\Generator */
    private $faker;
    /** @var  string */
    private $name;
    /** @var  string */
    private $password;

    public function _before(AcceptanceTester $I)
    {
        $this->faker = (new \League\FactoryMuffin\Faker\Faker())->getGenerator();
        $this->name = $this->faker->firstName;
        $this->password = $this->faker->password();
    }

    public function _after(AcceptanceTester $I)
    {
        /** @var \FOS\UserBundle\Doctrine\UserManager $userManager */
        $userManager = $I->grabService('fos_user.user_manager');
        $user = $userManager->findUserByUsername($this->name);
        if(null !== $user){
            $userManager->deleteUser($user);
        }
    }

    public function tryIndex(AcceptanceTester $I)
    {
        $I->amOnPage('/');
        $I->seeResponseCodeIs(200);
    }

    public function tryRegister(AcceptanceTester $I)
    {
        $I->amUser($this->name, $this->password);
        //Logout
        $I->amOnPage('/logout');
        $I->canSeeInCurrentUrl('/');

        $I->amLogin($this->name, $this->password);
    }

    public function tryCreateImage(AcceptanceTester $I)
    {
        $I->amUser($this->name, $this->password);

        $albumName = $this->faker->name;
        $I->createAlbum($albumName, $this->faker->text());

        //Image
        $I->click('Add image');
        $I->see('Create');
        $I->attachFile('Image', 'image.png');

        $imageName = $this->faker->title;
        $I->fillField('Name', $imageName);
        $I->fillField('Description', $this->faker->text());
        $I->click('Create');
        $I->see($imageName);

        //Check front page for public images
        $I->amOnPage('/');
        $I->canSeeElement('.thumbnail');
    }
}

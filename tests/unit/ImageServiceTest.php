<?php

use GalleryBundle\Entity\Album;
use GalleryBundle\Entity\Image;
use GalleryBundle\Service\ImageService;
use Gregwar\ImageBundle\Services\ImageHandling;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class ImageServiceTest extends \PHPUnit_Framework_TestCase
{
    /** @var string */
    private $imagesDirectory;
    /** @var  PHPUnit_Framework_MockObject_MockObject|ImageHandling */
    private $imageHandling;

    protected function setUp()
    {
        $this->imagesDirectory = 'test';
        $this->imageHandling = $this->getMockBuilder(ImageHandling::class)
                                    ->disableOriginalConstructor()
                                    ->getMock();
    }

    /**
     * @param array $methods
     * @return PHPUnit_Framework_MockObject_MockObject|ImageService
     */
    private function getImageService($methods = ['createThumbnail'])
    {
        return $this->getMockBuilder(ImageService::class)
                    ->setConstructorArgs([
                        $this->imagesDirectory,
                        $this->imageHandling,
                    ])
                    ->setMethods($methods)
                    ->getMock();
    }

    public function test_handleImage_calls()
    {
        $uploadedFile = $this->getMockBuilder(UploadedFile::class)->disableOriginalConstructor()->getMock();

        $uploadedFile->expects($this->exactly(2))->method('guessExtension')->willReturn('png');
        $uploadedFile->expects($this->once())->method('move');

        $this->getImageService()->handleImage(new Image(), $uploadedFile);
    }

    /**
     * @dataProvider filterPrivate_provider
     * @param $imageV
     * @param $album1V
     * @param $album2V
     * @param $expect
     */
    public function test_filterPrivate($imageV, $album1V, $album2V, $expect)
    {
        $album2 = new Album();
        $album2->setPrivate($album2V);

        $album1 = new Album();
        $album1->setPrivate($album1V);
        $album1->setParent($album2);

        $image = new Image();
        $image->setPrivate($imageV);
        $image->setAlbum($album1);

        $result = $this->getImageService()->filterPrivate([$image]);
        $this->assertEquals($expect, count($result));
    }

    public function filterPrivate_provider()
    {
        return [
            [true, true, true, 0],
            [true, false, false, 0],
            [true, true, false, 0],
            [true, false, true, 0],
            [false, true, true, 0],
            [false, false, false, 1],
            [false, true, false, 0],
            [false, false, true, 0],
        ];
    }
}

Gallery
====

To run project run this commands in project directory in sequence:
```sh
$ (cd vagrant/; vagrant up)
$ composer install
$ bin/console server:run
$ bin/console doctrine:database:create
$ bin/console doctrine:schema:update --force
```

Tests:
```sh
$ vendor/bin/codecept run
```

Link to project:
[http://localhost:8000/](http://localhost:8000/)
